/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comicsreader.userinfo;

import java.util.prefs.Preferences;

/**
 *
 * @author Tushar
 */
public class IDPass {
    Preferences p = Preferences.userNodeForPackage(IDPass.class);
    
    public void setIdPass(String id, String pass){
        p.put("id", id);
        p.put("pass", pass);
    }
    
    public void setLoggedIn(boolean b){
        p.putBoolean("loggedin", b);
    }
    
    public boolean getLoggedIn(){
        return p.getBoolean("loggedin", false);
    }
    
    public String getId(){
       return p.get("id", "null");
    }
    
    public String getPass(){
       return p.get("pass", "null");
    }
}
